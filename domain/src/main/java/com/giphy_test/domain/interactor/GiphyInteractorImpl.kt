package com.giphy_test.domain.interactor

import androidx.lifecycle.LiveData
import com.giphy_test.domain.gateway.GiphyApiGateway
import com.giphy_test.domain.models.GiphyResponse
import com.giphy_test.domain.state.GiphyState

class GiphyInteractorImpl(private val giphyGateway: GiphyApiGateway) : GiphyInteractor {
    override fun getGiphyResponseLiveData(): LiveData<GiphyState<GiphyResponse>> =
        giphyGateway.getGiphyResponseLiveData()

    override fun getGiphyResponse(searchString: String, offset: Int) =
        giphyGateway.getGiphyResponse(searchString, offset)
}