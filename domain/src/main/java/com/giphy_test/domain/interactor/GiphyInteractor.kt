package com.giphy_test.domain.interactor

import androidx.lifecycle.LiveData
import com.giphy_test.domain.models.GiphyResponse
import com.giphy_test.domain.state.GiphyState

interface GiphyInteractor {
    fun getGiphyResponseLiveData(): LiveData<GiphyState<GiphyResponse>>
    fun getGiphyResponse(searchString: String, offset: Int)
}