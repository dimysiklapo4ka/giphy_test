package com.giphy_test.domain.models

data class Images(
    val preview_gif: PreviewGif
)