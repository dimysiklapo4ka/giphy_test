package com.giphy_test.domain.models

data class Meta(
    val status: Int,
    val msg: String,
    val response_id: String
)