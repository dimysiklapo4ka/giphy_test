package com.giphy_test.domain.models

data class GiphyResponse(
    val `data`: List<Data>,
    val meta: Meta,
    val pagination: Pagination,
    val message: String? = null
)