package com.giphy_test.domain.models

data class Data(
    val id: String,
    val images: Images,
    val title: String,
    val url: String,
    val user: User,
    val username: String
)