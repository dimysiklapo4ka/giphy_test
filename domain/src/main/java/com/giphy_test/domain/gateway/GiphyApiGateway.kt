package com.giphy_test.domain.gateway

import androidx.lifecycle.LiveData
import com.giphy_test.domain.models.GiphyResponse
import com.giphy_test.domain.state.GiphyState

interface GiphyApiGateway {
    fun getGiphyResponseLiveData(): LiveData<GiphyState<GiphyResponse>>
    fun getGiphyResponse(searchString: String, offset: Int)
}