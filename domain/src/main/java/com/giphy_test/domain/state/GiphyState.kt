package com.giphy_test.domain.state

sealed class GiphyState<out T> {
    object InProgress : GiphyState<Nothing>()
    data class Success<T>(val data: T) : GiphyState<T>()
    data class InvalidApiKey(val message: String) : GiphyState<Nothing>()
    data class Error(val data: Throwable) : GiphyState<Nothing>()
}