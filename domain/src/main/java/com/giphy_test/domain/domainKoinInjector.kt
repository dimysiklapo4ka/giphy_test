package com.giphy_test.domain

import com.giphy_test.domain.interactor.GiphyInteractor
import com.giphy_test.domain.interactor.GiphyInteractorImpl
import org.koin.dsl.module

val domainInteractorInjector = module {
    single { GiphyInteractorImpl(get()) as GiphyInteractor }
}