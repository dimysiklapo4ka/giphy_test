package com.giphy_test.myapplication.extension

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.giphy_test.myapplication.listener.OnLoadMoreItem


fun RecyclerView.addOnLoadMoreItem(listener: OnLoadMoreItem) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager: LinearLayoutManager =
                recyclerView.layoutManager as LinearLayoutManager
            val visibleItemCount = layoutManager.childCount
            val totalItems = layoutManager.itemCount
            val totalItemCount = totalItems - 2
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

            if (!listener.isLoading() && totalItemCount > 0) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                    && firstVisibleItemPosition >= 0
                ) {
                    listener.onLoadMoreItem()
                }
            }

        }
    })
}