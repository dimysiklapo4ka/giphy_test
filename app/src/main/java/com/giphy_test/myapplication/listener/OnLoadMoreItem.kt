package com.giphy_test.myapplication.listener

interface OnLoadMoreItem {
    fun onLoadMoreItem()
    fun isLoading(): Boolean
}