package com.giphy_test.myapplication

import com.giphy_test.myapplication.activity.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appInjector = module {
    viewModel { MainViewModel(get()) }
}