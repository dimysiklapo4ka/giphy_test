package com.giphy_test.myapplication.app

import android.app.Application
import com.giphy_test.data.dataApiInjector
import com.giphy_test.data.dataRepositoryInjector
import com.giphy_test.domain.domainInteractorInjector
import com.giphy_test.myapplication.appInjector
import com.giphy_test.myapplication.livedata.InternetUtilLiveData
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class GiphyTestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@GiphyTestApplication)
            modules(
                listOf(
                    dataApiInjector,
                    dataRepositoryInjector,
                    domainInteractorInjector,
                    appInjector
                )
            )
        }
        InternetUtilLiveData.init(this)
    }

}