package com.giphy_test.myapplication.activity

import androidx.lifecycle.ViewModel
import com.giphy_test.domain.interactor.GiphyInteractor

class MainViewModel(
    private val giphyInteractor: GiphyInteractor
) : ViewModel() {

    fun getGiphyResponseLiveData() = giphyInteractor.getGiphyResponseLiveData()
    fun getGiphyResponse(searchString: String, offset: Int) =
        giphyInteractor.getGiphyResponse(searchString, offset)

    private var offset: Int = 0
    fun setOffset(offset: Int) {
        this.offset = offset
    }

    fun getOffset() = offset

    private var id: String = ""
    fun getId() = id
    fun setId(id: String) {
        this.id = id
    }
}