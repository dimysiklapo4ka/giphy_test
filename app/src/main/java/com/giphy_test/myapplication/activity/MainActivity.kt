package com.giphy_test.myapplication.activity

import android.content.res.Configuration
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.giphy_test.domain.models.GiphyResponse
import com.giphy_test.domain.state.GiphyState
import com.giphy_test.myapplication.R
import com.giphy_test.myapplication.adapter.GiphyAdapter
import com.giphy_test.myapplication.databinding.ActivityMainBinding
import com.giphy_test.myapplication.extension.addOnLoadMoreItem
import com.giphy_test.myapplication.listener.OnLoadMoreItem
import com.giphy_test.myapplication.livedata.InternetUtilLiveData
import com.giphy_test.myapplication.recycler_view.OffsetItemDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModel()
    private val adapter = GiphyAdapter()
    private var isLoading = false

    private val giphyObserver = Observer<GiphyState<GiphyResponse>> {
        when (it) {
            is GiphyState.InProgress -> {
                isLoading = true
                binding.loading = true
            }
            is GiphyState.Success -> successGiphyResponse(it.data)
            is GiphyState.Error -> errorGiphyResponse(getString(R.string.some_error_message))
            is GiphyState.InvalidApiKey -> errorGiphyResponse(getString(R.string.invalid_api_key_error_message))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        observeLiveData(viewModel)

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.apply {
                    if (this.isNotEmpty() && this.length > 3) {
                        clearAdapter()
                        viewModel.getGiphyResponse(this, viewModel.getOffset())
                    } else clearAdapter()
                } ?: clearAdapter()
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }
        })
    }

    override fun onStart() {
        super.onStart()
        when (resources.configuration.orientation) {
            Configuration.ORIENTATION_PORTRAIT ->
                binding.gifList.layoutManager = GridLayoutManager(this, 2)
            Configuration.ORIENTATION_LANDSCAPE ->
                binding.gifList.layoutManager = GridLayoutManager(this, 4)
            else -> binding.gifList.layoutManager = GridLayoutManager(this, 3)
        }
        binding.gifList.addOnLoadMoreItem(object : OnLoadMoreItem {
            override fun onLoadMoreItem() {
                viewModel.getGiphyResponse(
                    binding.searchView.query.toString(),
                    viewModel.getOffset()
                )
            }

            override fun isLoading(): Boolean = isLoading
        })
        binding.gifList.adapter = adapter
        binding.gifList.addItemDecoration(OffsetItemDecoration(16, 58, 16, 16, 8))
    }

    private var connected = true
    fun observeLiveData(viewModel: MainViewModel) {
        with(viewModel) {
            getGiphyResponseLiveData().observe(this@MainActivity, giphyObserver)
            InternetUtilLiveData.observe(this@MainActivity, Observer {
                if (it != connected) {
                    internetConnection(it)
                    connected = it
                }
            })
        }
    }

    private fun internetConnection(connection: Boolean) {
        if (connection) Toast.makeText(
            this,
            getString(R.string.internet_connect),
            Toast.LENGTH_SHORT
        ).show()
        else Toast.makeText(this, getString(R.string.internet_disconnect), Toast.LENGTH_SHORT)
            .show()
    }

    private fun clearAdapter() {
        adapter.clearAdapter()
        viewModel.setOffset(0)
    }

    private fun loadingFalse() {
        isLoading = false
        binding.loading = false
    }

    private fun successGiphyResponse(data: GiphyResponse) {
        if (!data.meta.response_id.equals(viewModel.getId())) {
            viewModel.setOffset(data.pagination.offset + data.pagination.count)
            viewModel.setId(data.meta.response_id)
            adapter.addData(data.data)
        }
        loadingFalse()
    }

    private fun errorGiphyResponse(message: String) {
        loadingFalse()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}