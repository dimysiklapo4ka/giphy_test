package com.giphy_test.myapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.giphy_test.domain.models.Data
import com.giphy_test.myapplication.databinding.ItemGiphyBinding

class GiphyAdapter : RecyclerView.Adapter<GiphyAdapter.GiphyViewHolder>() {

    private val data = mutableListOf<Data>()

    fun addData(responseData: List<Data>) {
        data.addAll(responseData)
        notifyDataSetChanged()
    }

    fun clearAdapter() {
        data.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiphyViewHolder {
        return GiphyViewHolder(
            ItemGiphyBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: GiphyViewHolder, position: Int) {
        holder.bindViewHolder(data[position])
    }

    class GiphyViewHolder(private val binding: ItemGiphyBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindViewHolder(data: Data) {
            binding.apply {
                giphyData = data
                executePendingBindings()
            }
        }

    }

}