package com.giphy_test.myapplication.recycler_view

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.giphy_test.myapplication.extension.dpToPx

class OffsetItemDecoration(l: Int, t: Int, r: Int, b: Int, beetwenItemOffset: Int) :
    RecyclerView.ItemDecoration() {

    private val left = l.dpToPx()
    private val top = t.dpToPx()
    private val right = r.dpToPx()
    private val bottom = b.dpToPx()
    private val beetwen = beetwenItemOffset.dpToPx()

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {

        val itemCount = parent.adapter?.itemCount ?: 0
        val layoutParams = view.layoutParams as GridLayoutManager.LayoutParams
        val spanCount = (parent.layoutManager as GridLayoutManager).spanCount
        val itemPosition = parent.getChildLayoutPosition(view)

        if (itemCount > spanCount) {
            if (itemPosition < spanCount) {
                if (layoutParams.spanIndex + 1 == spanCount)
                    outRect.set(beetwen / 2, top, right, beetwen / 2)
                else {
                    if (layoutParams.spanIndex + 1 == 1) {
                        outRect.set(left, top, beetwen / 2, beetwen / 2)
                    } else outRect.set(beetwen / 2, top, beetwen / 2, beetwen / 2)
                }
            } else {
                if (itemPosition < itemCount - spanCount) {
                    if (layoutParams.spanIndex + 1 == spanCount)
                        outRect.set(beetwen / 2, beetwen / 2, right, beetwen / 2)
                    else {
                        if (layoutParams.spanIndex + 1 == 1) {
                            outRect.set(left, beetwen / 2, beetwen / 2, beetwen / 2)
                        } else outRect.set(beetwen / 2, beetwen / 2, beetwen / 2, beetwen / 2)
                    }
                } else {
                    if (layoutParams.spanIndex + 1 == spanCount)
                        outRect.set(beetwen / 2, beetwen / 2, right, bottom)
                    else {
                        if (layoutParams.spanIndex + 1 == 1) {
                            outRect.set(left, beetwen / 2, beetwen / 2, bottom)
                        } else outRect.set(beetwen / 2, beetwen / 2, beetwen / 2, bottom)
                    }
                }
            }
        } else {
            if (layoutParams.spanIndex + 1 == spanCount)
                outRect.set(beetwen / 2, top, right, bottom)
            else {
                if (layoutParams.spanIndex + 1 == 1) {
                    outRect.set(left, top, beetwen / 2, bottom)
                } else outRect.set(beetwen / 2, top, beetwen / 2, bottom)
            }
        }
    }

}

