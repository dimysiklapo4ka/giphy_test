package com.giphy_test.myapplication.binding_adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.giphy_test.domain.models.PreviewGif

@BindingAdapter("app:gifImage")
fun setPaddingLeft(view: ImageView, data: PreviewGif) {
    Glide.with(view.context)
        .asGif()
        .load(data.url)
        .into(view)
}
