package com.giphy_test.myapplication.binding_adapter

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("app:visible")
fun View.visible(visibility: Boolean) {
    if (visibility) this.visibility = View.VISIBLE
    else this.visibility = View.GONE
}