package com.giphy_test.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.giphy_test.data.api.GiphyApi
import com.giphy_test.domain.gateway.GiphyApiGateway
import com.giphy_test.domain.models.GiphyResponse
import com.giphy_test.domain.state.GiphyState
import kotlinx.coroutines.*

class GiphyApiRepository(private val giphyApi: GiphyApi) : GiphyApiGateway {

    private val giphyLiveData = MutableLiveData<GiphyState<GiphyResponse>>()
    override fun getGiphyResponseLiveData(): LiveData<GiphyState<GiphyResponse>> = giphyLiveData

    private var giphySearchJob: Job? = null
    override fun getGiphyResponse(searchString: String, offset: Int) {
        giphySearchJob?.cancel()
        giphySearchJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                giphyLiveData.postValue(GiphyState.InProgress)
                delay(1000)
                val result = giphyApi.requestGiphyBySearchString(
                    searchString = searchString,
                    offset = offset
                ).await()
                if (result.isSuccessful) {
                    result.body()?.let {
                        if (it.message == null) giphyLiveData.postValue(GiphyState.Success(it))
                        else giphyLiveData.postValue(GiphyState.InvalidApiKey(it.message ?: ""))
                    } ?: giphyLiveData.postValue(GiphyState.Error(Throwable(result.message())))
                } else giphyLiveData.postValue(GiphyState.Error(Throwable(result.message())))
            } catch (e: Throwable) {
                giphyLiveData.postValue(GiphyState.Error(e))
            }
        }
    }

}