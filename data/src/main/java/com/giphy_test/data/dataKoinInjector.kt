package com.giphy_test.data

import com.giphy_test.data.api.GiphyApi
import com.giphy_test.data.repository.GiphyApiRepository
import com.giphy_test.data.retrofit.RetrofitClient
import com.giphy_test.domain.gateway.GiphyApiGateway
import org.koin.dsl.module

val dataApiInjector = module {
    single {
        RetrofitClient().getRestInterface(BuildConfig.WEB_SERVER_GIPHY)
            .create(GiphyApi::class.java) as GiphyApi
    }
}

val dataRepositoryInjector = module {
    single { GiphyApiRepository(get()) as GiphyApiGateway }
}